#
# This Dockerfile is used to build the final production image.
# It contains the WHOIS scraper that scrapes the RADb and ARIN databases.
#

#
# The build stage which builds the application.
#

FROM gradle:alpine AS gradle

# Set the working directory to /srv/whois/
WORKDIR /srv/whois/

# Set the current user of the image to 'root'
USER root

# Copy over the project to the Dockerfile
COPY . /srv/whois/

# Create a java executable by running installDist
RUN gradle installDist

#
# The final stage of the Multistage Docker image.
# By using multistage builds we wipe everything from before clean.
#

FROM openjdk:alpine

# Set the working directory again.
WORKDIR /srv/whois/

# Copy over the resources and the binary executable.
COPY --from=gradle /srv/whois/res/ /srv/whois/res/
COPY --from=gradle /srv/whois/build/install/whois/ /srv/whois/

# Run the WHOIS program.
CMD ["/srv/whois/bin/whois"]
