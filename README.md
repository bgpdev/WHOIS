# README.md
## Introduction
The WHOIS project is aimed at storing WHOIS records locally so they can be retrieved quicker without placing a burden on the WHOIS servers.
The project contains a WHOIS scraper that can scrape the contents of RADb and ARIN.
Since RADb already mirrors all irr's (the ARIN whois database is not in sync with the ARIN irr database that RADb uses so that is why we will parse it separately as well.)
A Dockerfile is present to run the application easily. See "Configuring the WHOIS scraper" for more details.

If any issues are found, please submit them at Gitlab.

## Initializing a local Elastic Search database.
If you do not have an external Elasticsearch database that you want to use,
you can setup a local Elasticsearch database by using the docker-compose.yml already present in the repository.

To start Elasticsearch and Kibana (The web GUI for Elasticsearch)

```
docker-compose up -d elasticsearch
docker-compose up -d kibana
```

Elasticsearch is configured to run on port 9200 and Kibana on port 5601.
To change these ports or the configuration of your local setup, change the docker-compose.yml file locally.

**Note:** If you are not using a local Elasticsearch server but rather an external one,
you can skip these steps and use the ELASTICSEARCH_URL environment variable on the WHOIS scraper to have it insert records in the external database.


## Configuring the WHOIS scraper
The WHOIS Scraper can be configured by setting environment variables.
The most easiest way to configure and run the WHOIS Scraper is to use the docker-compose.yml file that is included in the project.
The docker-compose.yml file already has sensible defaults but can be altered to change behavior.
The following environment variables are defined:

```
ELASTICSEARCH_URL: <URL to Elastic Search>:<port>, i.e http://elasticsearch.domain.com:9200
```
```
ARIN_FALLBACK: Whether to use the ARIN WHOIS service as a fallback when RADb does not have any records. i.e true or false
```
```
RADB_CONNECTIONS: <The amount of persistent RADb connections to use in parallel>, i.e 5
```
Note: An email from RADb told us that we can open up to 5 connections at a single time.

```
RADB_RECONNECT_ATTEMPTS: <The amount of times the RADbresolver will try to reconnect upon network errors>, i.e 3
```
```
RADB_RECONNECT_DELAY: <The amount milliseconds to wait between each reconnect attempt when querying RADb>, i.e 10000
```
```
WHOIS_RECONNECT_ATTEMPTS: <The amount of times the generic WHOIS resolver (used for ARIN requests) will try to reconnect upon network errors>, i.e 3
```
```
WHOIS_RECONNECT_DELAY: <The amount milliseconds to wait between each reconnect attempt for the generic WHOIS resolver (used for ARIN requests)>, i.e 15000
```
```
THREADPOOL_SIZE: <The amount of threads that will be used to do requests.>, i.e 50
Note: It is recommended that if you use an ARIN Fallback,
to use a large pool of threads because the delay in each REST request is significant.
When only using the RADb resolvers, between 5 and 10 should be fine.
```
```
RIR_EXCHANGE_SPECIFICATIONS: <Switch that configures the scraper to use the RIR Exchange Specifications as datasource> i.e true or false.
```
```
THYME_APNIC_LISTINGS: <Switch that configures the scraper to use the Thyme APNIC Listings as datasource> i.e true or false.
```

To run the WHOIS scraper from the docker-compose.yml file, use the following commands.
The scraper will use sensible defaults if the environment variables specified above were not already set.

```
docker-compose up -d scraper
```

To run the WHOIS scraper with Docker only:
```
docker build -t whois_scraper .
docker run -d -e <ENV_VAR_HERE>=<VALUE> whois_scraper
```

To specify multiple environment variables use multiple -e arguments.