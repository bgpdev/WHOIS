package utility;

import org.apache.commons.io.FileUtils;
import po.POWriter;
import rpsl.RPSLObject;
import rsef.RSEFObject;
import rsef.RSEFReader;
import rsef.RSEFRecord;
import whois.resolver.generic.GenericResolver;

import java.io.DataOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.net.URL;
import java.util.ArrayList;

public class LACNIC2PO
{
    private static long nLACNIC = 0;
    private static long rate = 20000;

    /**
     * Scrapes LACNIC IPv4 prefixes and converts them to a PO file.
     * @param argv Any parameters passed to the program, currently unused.
     * @throws Exception Any exceptions that occur.
     */
    public static void main(String[] argv) throws Exception
    {
        /* The amount of collectors parsing this file */
        Long amount = Long.parseLong(argv[0]);
        Long ID = Long.parseLong(argv[1]);
        String url = argv[2];

        File tmp = File.createTempFile("rsef", ".tmp");
        FileUtils.copyURLToFile(new URL(url), tmp);

        RSEFReader reader = new RSEFReader(new FileInputStream(tmp));
        while(!reader.isEOF())
        {
            RSEFObject object = reader.read();
            if (object instanceof RSEFRecord)
            {
                RSEFRecord record = (RSEFRecord) object;
                if((nLACNIC++ + ID) % amount != 0 || record.status.equals("available") || record.status.equals("reserved"))
                    continue;

                try
                {
                    System.out.println("New block: " + record.start + "(" + record.value + ")");

                    // Create PO file.
                    File po = new File("LACNIC/" + record.start.replaceAll(":", "_") + "(" + record.value + ").po");
                    if (po.exists())
                        continue;

                    po.getParentFile().mkdirs();
                    po.createNewFile();

                    DataOutputStream stream = new DataOutputStream(new FileOutputStream(po, true));
                    POWriter writer = new POWriter(stream);
                    scrape(writer, record.start, "whois.lacnic.net");

                    stream.close();
                }
                catch (Exception e)
                {
                    e.printStackTrace();
                }
            }
        }
    }

    public static void scrape(POWriter writer, String prefix, String registry)
    {
        try
        {
            GenericResolver resolver = new GenericResolver(registry);
            Thread.sleep(rate);
            ArrayList<RPSLObject> results = resolver.query(prefix);

            if(results.isEmpty())
                return;

            for(RPSLObject x : results)
            {
                try
                {
                    // In case ARIN results are retrieved without route object.
                    if(!x.hasProperty("inetnum") || x.getProperty("inetnum").contains("-"))
                        break;

                    // Calculate intermediary values
                    String x_prefix = x.getProperty("inetnum").trim();
                    int x_length = Integer.parseInt(x_prefix.split("/")[1]);

                    if(x.hasProperty("aut-num") && x.getProperty("aut-num").matches("AS[0-9]+"))
                    {
                        System.out.println("New pair: (" + x_prefix + ", " + x.getProperty("aut-num") + ")");
                        writer.write(x_prefix.split("/")[0], x_length, Long.parseLong(x.getProperty("aut-num").replace("AS", "")));
                    }
                }
                catch(Exception e)
                {
                    System.out.println("Could not parse prefix or ASN, skipping this entry.");
                    e.printStackTrace();
                }
            }
        }
        catch(Exception e)
        {
            e.printStackTrace();
        }
    }
}