package system;

import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.URL;

public class ElasticSearch
{
    // The URL to the Elasticsearch database.
    private String host;

    /**
     * Constructs an object that is used to connect to Elasticsearch.
     * @param host The base url of the Elasticsearch service.
     * @throws Exception Exception for connectivity issues
     */
    public ElasticSearch(String host) throws Exception
    {
        this.host = host;
        URL url = new URL(host + "/_cat/health");
        BufferedReader reader = new BufferedReader(new InputStreamReader(url.openStream()));
        System.out.println("Elastic Search status: ");
        reader.lines().forEach(System.out::println);
    }

    /**
     * Performs a GET operation on the Elastic Search Database.
     * @param path The path being queried, i.e. host/path
     * @return JsonObject - The response that has been returned by Elastic Search.
     * @throws Exception Exception for connectivity issues
     */
    public JsonObject get(String path) throws Exception
    {
        URL url = new URL(host + "/" + path);

        HttpURLConnection connection = (HttpURLConnection) url.openConnection();
        connection.setDoOutput(false);
        connection.setRequestMethod("GET");

        InputStream stream;
        if (connection.getResponseCode() < HttpURLConnection.HTTP_BAD_REQUEST)
            stream = connection.getInputStream();
        else
            stream = connection.getErrorStream();

        JsonElement result = new JsonParser().parse(new InputStreamReader(stream, "UTF-8"));

        return result.getAsJsonObject();
    }

    /**
     * Performs a HEAD operation on the Elastic Search Database.
     * @param path The path being queried, i.e. host/path
     * @return JsonObject - The response that has been returned by Elastic Search.
     * @throws Exception Exception for connectivity issues
     */
    public JsonObject head(String path) throws Exception
    {
        URL url = new URL(host + "/" + path);

        HttpURLConnection connection = (HttpURLConnection) url.openConnection();
        connection.setDoOutput(false);
        connection.setRequestMethod("HEAD");

        InputStream stream;
        if (connection.getResponseCode() < HttpURLConnection.HTTP_BAD_REQUEST)
            stream = connection.getInputStream();
        else
            stream = connection.getErrorStream();

        JsonElement result = new JsonParser().parse(new InputStreamReader(stream, "UTF-8"));

        return result.getAsJsonObject();
    }


    /**
     * Performs a GET operation with an Object body on the Elastic Search Database.
     * @param path The path being queried, i.e. host/path
     * @param object The request being made.
     * @return JsonObject - The response that has been returned by Elastic Search.
     * @throws Exception Exception for connectivity issues
     */
    public JsonObject get(String path, JsonObject object) throws Exception
    {
        URL url = new URL(host + "/" + path);

        HttpURLConnection connection = (HttpURLConnection) url.openConnection();
        connection.setDoOutput(true);
        connection.setRequestMethod("GET");
        connection.setRequestProperty("Content-Type", "application/json");
        OutputStreamWriter out = new OutputStreamWriter(connection.getOutputStream());
        out.write(object.toString());
        out.close();

        InputStream stream;
        if (connection.getResponseCode() < HttpURLConnection.HTTP_BAD_REQUEST)
            stream = connection.getInputStream();
        else
            stream = connection.getErrorStream();

        JsonElement result = new JsonParser().parse(new InputStreamReader(stream, "UTF-8"));

        return result.getAsJsonObject();
    }

    /**
     * Performs a PUT operation on the Elastic Search Database.
     * @param path The path being queried, i.e. host/path
     * @param object The request being made.
     * @return JsonObject - The response that has been returned by Elastic Search.
     * @throws Exception Exception for connectivity issues
     */
    public JsonObject put(String path, JsonObject object) throws Exception
    {
        URL url = new URL(host + "/" + path);

        HttpURLConnection connection = (HttpURLConnection) url.openConnection();
        connection.setDoOutput(true);
        connection.setRequestMethod("PUT");
        connection.setRequestProperty("Content-Type", "application/json");
        OutputStreamWriter out = new OutputStreamWriter(connection.getOutputStream());
        out.write(object.toString());
        out.close();

        InputStream stream;
        if (connection.getResponseCode() < HttpURLConnection.HTTP_BAD_REQUEST)
            stream = connection.getInputStream();
        else
            stream = connection.getErrorStream();

        JsonElement result = new JsonParser().parse(new InputStreamReader(stream, "UTF-8"));

        return result.getAsJsonObject();
    }

    /**
     * Performs a POST operation on the Elastic Search Database.
     * @param path The path being queried, i.e. host/path
     * @param object The request being made.
     * @return JsonObject - The response that has been returned by Elastic Search.
     * @throws Exception Exception for connectivity issues
     */
    public JsonObject post(String path, JsonObject object) throws Exception
    {
        URL url = new URL(host + "/" + path);

        HttpURLConnection connection = (HttpURLConnection) url.openConnection();
        connection.setDoOutput(true);
        connection.setRequestMethod("POST");
        connection.setRequestProperty("Content-Type", "application/json");
        OutputStreamWriter out = new OutputStreamWriter(connection.getOutputStream());
        out.write(object.toString());
        out.close();

        InputStream stream;
        if (connection.getResponseCode() < HttpURLConnection.HTTP_BAD_REQUEST)
            stream = connection.getInputStream();
        else
            stream = connection.getErrorStream();

        JsonElement result = new JsonParser().parse(new InputStreamReader(stream, "UTF-8"));

        return result.getAsJsonObject();
    }

    /**
     * Performs a DELETE operation on the Elastic Search Database.
     * @param path The path being queried, i.e. host/path
     * @return JsonObject - The response that has been returned by Elastic Search.
     * @throws Exception Exception for connectivity issues
     */
    public JsonObject delete(String path) throws Exception
    {
        URL url = new URL(host + "/" + path);

        HttpURLConnection connection = (HttpURLConnection) url.openConnection();
        connection.setRequestMethod("DELETE");
        connection.setRequestProperty("Content-Type", "application/json");

        InputStream stream;
        if (connection.getResponseCode() < HttpURLConnection.HTTP_BAD_REQUEST)
            stream = connection.getInputStream();
        else
            stream = connection.getErrorStream();

        JsonElement result = new JsonParser().parse(new InputStreamReader(stream, "UTF-8"));

        return result.getAsJsonObject();
    }

}
