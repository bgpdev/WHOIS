package system;

/**
 * Configuration class that holds all global variables.
 */
public class Configuration
{
    /** The Singleton configuration class. */
    private static Configuration configuration = null;

    public final int RADB_RECONNECT_ATTEMPTS;

    public final int RADB_RECONNECT_DELAY;

    public final String ELASTICSEARCH_URL;

    public final int WHOIS_RECONNECT_DELAY;

    public final int WHOIS_RECONNECT_ATTEMPTS;

    /**
     * Constructs a Configuration object.
     */
    private Configuration()
    {
        /* --------------------------------------------
         * Verify if the environment variables are set
         * --------------------------------------------*/
        RADB_RECONNECT_ATTEMPTS = System.getenv("RADB_RECONNECT_ATTEMPTS") != null ? Integer.parseInt(System.getenv("RADB_RECONNECT_ATTEMPTS")) : 10;
        RADB_RECONNECT_DELAY = System.getenv("RADB_RECONNECT_DELAY") != null ? Integer.parseInt(System.getenv("RADB_RECONNECT_DELAY")) : 10000;
        ELASTICSEARCH_URL = System.getenv("ELASTICSEARCH_URL") != null ? System.getenv("ELASTICSEARCH_URL") : "http://localhost:9200";
        WHOIS_RECONNECT_ATTEMPTS = System.getenv("WHOIS_RECONNECT_ATTEMPTS") != null ? Integer.parseInt(System.getenv("WHOIS_RECONNECT_ATTEMPTS")) : 10;
        WHOIS_RECONNECT_DELAY = System.getenv("WHOIS_RECONNECT_DELAY") != null ? Integer.parseInt(System.getenv("WHOIS_RECONNECT_DELAY")) : 1000;
    }

    /**
     * Retrieves the global Configuration object.
     * @return The global Configuration object.
     */
    public static Configuration getConfig()
    {
        if(configuration == null)
            configuration = new Configuration();
        return configuration;
    }
}
