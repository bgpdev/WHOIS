package po;

import java.io.DataOutputStream;
import java.net.InetAddress;
import java.nio.ByteBuffer;
import java.util.ArrayList;
import java.util.Set;

public class POWriter
{
    private DataOutputStream output;

    public POWriter(DataOutputStream output)
    {
        this.output = output;
    }

    public void write(String prefix, int length, long origin)
    {
        try
        {
            // Write a byte with value 0 to indicate that it is an IPv4 prefix.
            if(prefix.contains("."))
                output.writeByte(0);
            else
                output.writeByte(1);

            // Write the prefix length.
            output.writeByte(length);

            // Write the prefix.
            output.write(InetAddress.getByName(prefix).getAddress());

            // Write the origin.
            output.write(ByteBuffer.allocate(4).putInt((int)origin).array());
        }
        catch(Exception e)
        {
            System.out.println("Skipping malformed route entry...");
            e.printStackTrace();
        }
    }
}
