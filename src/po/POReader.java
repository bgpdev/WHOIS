package po;

import java.io.DataInputStream;
import java.net.InetAddress;

/**
 * Class to read .po (Prefix-Origin) files.
 */
public class POReader
{
    private DataInputStream input;

    public POReader(DataInputStream input)
    {
        this.input = input;
    }

    public PO read() throws Exception
    {
        PO object = new PO();

        while(input.available() > 0)
        {
            int type = input.readUnsignedByte();
            int prefix = input.readUnsignedByte();

            if (type == 0)
            {
                byte[] buffer = new byte[4];
                input.readFully(buffer);
                object.prefix = InetAddress.getByAddress(buffer).getHostAddress() + "/" + prefix;
            }
            else
            {
                byte[] buffer = new byte[16];
                input.readFully(buffer);
                object.prefix = InetAddress.getByAddress(buffer).getHostAddress() + "/" + prefix;
            }

            object.origin = Integer.toUnsignedLong(input.readInt());

            return object;
        }

        return null;
    }
}
