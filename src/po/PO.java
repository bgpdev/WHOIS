package po;

/**
 * An PO object is a pair of prefix and ASN.
 */
public class PO
{
    /** The prefix that may be announced by the origin. */
    public String prefix;

    /** The origin that may announce this prefix. */
    public Long origin;
}