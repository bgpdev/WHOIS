package whois.resolver;

import rpsl.RPSLObject;

import java.util.ArrayList;

/**
 * All WHOIS Resolvers should use this class as base class.
 */
public abstract class Resolver
{
    public abstract ArrayList<RPSLObject> query(String query) throws Exception;
}
