package whois.resolver.radb;

import rpsl.RPSLObject;
import rpsl.RPSLReader;
import system.Configuration;
import whois.resolver.Resolver;

import java.io.*;
import java.net.Socket;
import java.util.ArrayList;
import java.util.Date;

/**
 * The RADBResolver object constructs a persistent connection with the RADb database upon creation.
 */
public class RADBResolver extends Resolver
{
    /** The input stream from client perspective. The client reads from this. */
    private DataInputStream input;

    /** The output stream from client perspective.. The client writes to this. */
    private DataOutputStream output;

    /** Represents the socket that is being used for the connection to the WHOIS server. */
    private Socket socket;

    /** A temporary buffer of 200000 bytes that is used to store the results. */
    private final int BUFFER_SIZE = 2000000;
    private byte[] buffer = new byte[BUFFER_SIZE];

    /**
     * Creates a RIPEResolver that connects to a WHOIS server.
     * @throws Exception Any Exceptions that occur while connecting.
     */
    public RADBResolver() throws Exception
    {
        connect();
    }

    /**
     * Open a permanent connection to the RADb WHOIS server.
     * @throws Exception Thrown when the max reconnect attemps are exceeded.
     */
    private void connect() throws Exception
    {
        for(int i = 0; i < Configuration.getConfig().RADB_RECONNECT_ATTEMPTS; i++)
        {
            try
            {
                socket = new Socket("whois.radb.net", 43);
                output = new DataOutputStream(socket.getOutputStream());
                input = new DataInputStream(socket.getInputStream());

                // Create a persistent connection to the RABD WHOIS server.
                output.writeBytes("!!\n");
                return;
            }
            catch (Exception e)
            {
                System.out.println("[" + new Date().toString() + "] RADB: An error occurred while trying to reconnect: " + e.getMessage());
                System.out.println("[" + new Date().toString() + "] RADB: Retrying in " + Configuration.getConfig().RADB_RECONNECT_DELAY);
                Thread.sleep(Configuration.getConfig().RADB_RECONNECT_DELAY);
            }
        }

        throw new Exception("RADB: Max reconnect attempts exceeded.");
    }

    /**
     * Sends a new request and waits for the result.
     * Note: The method is async such that multiple queries are not being made concurrently,
     * as that could mess up the output.
     * @param query The query that should be executed.
     */
    public synchronized ArrayList<RPSLObject> query(String query) throws Exception
    {
        for(int i = 0; i < Configuration.getConfig().RADB_RECONNECT_ATTEMPTS; i++)
        {
            try
            {
                // Send the query to the RADb server.
                output.writeBytes(query + '\n');

                int index = 0;
                while (true)
                {
                    // Read data into the temporary buffer.
                    buffer[index++] = input.readByte();

                    // If three newlines are found, the end of this query has been reached. Return RSPLObjects.
                    if (index >= 3 && (buffer[index - 3] == '\n' && buffer[index - 2] == '\n' && buffer[index - 1] == '\n'))
                        return new RPSLReader(new ByteArrayInputStream(buffer, 0, index - 1)).readAll();
                }
            }
            catch (Exception e)
            {
                e.printStackTrace();
                System.out.println("[" + new Date().toString() + "] RADB: An error occurred: " + e.getMessage());
                System.out.println("[" + new Date().toString() + "] RADB: Reconnecting in: " + Configuration.getConfig().RADB_RECONNECT_DELAY);
                e.printStackTrace();
                connect();
            }
        }

        throw new Exception("RADB: Max reconnect attempts exceeded.");
    }

    /**
     * Closes the connection with the RADb server.
     * @throws IOException An exception that might be thrown when closing the GenericResolver socket.
     */
    public void close() throws IOException
    {
        socket.close();
    }
}
