package whois.resolver.arin;

import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import inet.ipaddr.IPAddress;
import inet.ipaddr.IPAddressString;
import rpsl.RPSLObject;

import java.net.InetAddress;
import java.util.ArrayList;
import java.util.Map;

/**
 * Contains ARIN Utility functions for converting an WHOIS ARIN result to a RADb format.
 */
public class ARINUtility
{
    /**
     * Converts an ARIN WHOIS key-named RPSLObject to a RADb key-named RPSL object.
     * It filters out properties that are not convertable to RADb format.
     * @param handles The response of a query to the ARIN WHOIS service.
     * @return The RADb key-named formatted RPSLObject
     */
    static RPSLObject convertToRPSL(ArrayList<RPSLObject> handles)
    {
        RPSLObject filtered = new RPSLObject();
        filtered.addProperty("source", "ARIN-Filtered");
        for(RPSLObject arin : handles)
        {
            for(Map.Entry<String, JsonElement> x : arin.toJson().entrySet())
            {
                switch(x.getKey())
                {
                    case "CIDR":
                        if(x.getValue().getAsString().contains("."))
                            filtered.addProperty("route", x.getValue().getAsString());
                        else
                            filtered.addProperty("route6", x.getValue().getAsString());
                        break;
                    case "OriginAS":
                        filtered.addProperty("origin", x.getValue().getAsString());
                        break;
                    case "ASNumber":
                        filtered.addProperty("aut-num", "AS" + x.getValue().getAsString());
                        break;
                    case "ASName":
                        filtered.addProperty("as-name", x.getValue().getAsString());
                        break;
                    case "RegDate":
                        filtered.addProperty("created", x.getValue().getAsString());
                        break;
                    case "Updated":
                        filtered.addProperty("changed", x.getValue().getAsString());
                        filtered.addProperty("last-modified", x.getValue().getAsString());
                        break;
                    case "OrgId":
                        filtered.addProperty("org", x.getValue().getAsString());
                        break;
                    case "OrgName":
                        filtered.addProperty("descr", x.getValue().getAsString());
                        break;
                    case "Comment":
                        filtered.addProperty("remarks", x.getValue().getAsString());
                        break;
                }
            }
        }

        return filtered;
    }

    /**
     * Converts an ARIN WHOIS key-named RPSLObject to a RADb key-named RPSL object.
     * It filters out properties that are not convertable to RADb format.
     * @param object The response of a query to the ARIN WHOIS service.
     * @return The RADb key-named formatted RPSLObject
     */
    static RPSLObject convertToRPSL(RPSLObject object) throws Exception
    {
        RPSLObject filtered = new RPSLObject();
        filtered.addProperty("source", "ARIN-Filtered");
        for(Map.Entry<String, JsonElement> x : object.toJson().entrySet())
        {
            switch(x.getKey())
            {
                case "CIDR":
                    if(x.getValue().getAsString().contains("."))
                        filtered.addProperty("route", x.getValue().getAsString());
                    else
                        filtered.addProperty("route6", x.getValue().getAsString());
                    break;
                case "NetRange":
                    String[] split = x.getValue().getAsString().split(" - ");
                    IPAddress start = new IPAddressString(split[0]).toAddress();
                    IPAddress end = new IPAddressString(split[1]).toAddress();
                    IPAddress prefix = start.spanWithPrefixBlocks(end)[0];
                    if(prefix.toString().contains("/"))
                    {
                        if (prefix.toString().contains("."))
                            filtered.addProperty("route", prefix.toString());
                        else
                            filtered.addProperty("route6", prefix.toString());
                    }
                    break;
                case "OriginAS":
                    filtered.addProperty("origin", x.getValue().getAsString());
                    break;
                case "ASNumber":
                    filtered.addProperty("aut-num", "AS" + x.getValue().getAsString());
                    break;
                case "ASName":
                    filtered.addProperty("as-name", x.getValue().getAsString());
                    break;
                case "RegDate":
                    filtered.addProperty("created", x.getValue().getAsString());
                    break;
                case "Updated":
                    filtered.addProperty("changed", x.getValue().getAsString());
                    filtered.addProperty("last-modified", x.getValue().getAsString());
                    break;
                case "OrgId":
                    filtered.addProperty("org", x.getValue().getAsString());
                    break;
                case "OrgName":
                    filtered.addProperty("descr", x.getValue().getAsString());
                    break;
                case "Comment":
                    filtered.addProperty("remarks", x.getValue().getAsString());
                    break;
            }
        }

        return filtered;
    }

    /**
     * Converts an ARIN REST API response to a RADb key-named RPSL object.
     * It filters out properties that are not convertable to RADb format.
     * @param response The response of a query to the ARIN REST service.
     * @return The RADb key-named formatted RPSLObject
     */
    static ArrayList<RPSLObject> convertToRPSL(JsonObject response) throws Exception
    {
        ArrayList<RPSLObject> results = new ArrayList<>();
        RPSLObject object = new RPSLObject();
        object.addProperty("source", "ARIN-Filtered");

        if(response.has("asn"))
            response = response.get("asn").getAsJsonObject();
        if(response.has("net"))
            response = response.get("net").getAsJsonObject();

        if(response.has("registrationDate"))
            object.addProperty("created", response.get("registrationDate").getAsJsonObject().get("$").getAsString());

        if(response.has("orgRef"))
        {
            JsonObject ref = response.get("orgRef").getAsJsonObject();
            object.addProperty("org", ref.get("@handle").getAsString());
            object.addProperty("org-name", ref.get("@name").getAsString());
        }

        if(response.has("updateDate"))
            object.addProperty("last-modified", response.get("updateDate").getAsJsonObject().get("$").getAsString());

        // -------------------
        // ASN Related
        // -------------------

        if(response.has("name"))
            object.addProperty("as-name", response.get("name").getAsJsonObject().get("$").getAsString());

        if(response.has("startAsNumber") && response.has("endAsNumber"))
        {
            long start = Long.parseLong(response.get("startAsNumber").getAsJsonObject().get("$").getAsString());
            long end = Long.parseLong(response.get("endAsNumber").getAsJsonObject().get("$").getAsString());
            for(long i = start; i <= end; i++)
            {
                RPSLObject x = new RPSLObject(object.toJson().toString());
                x.addProperty("aut-num", "AS" + i);
                results.add(x);
            }

            return results;
        }

        // -------------------
        // prefix Related
        // -------------------

        if(response.has("originASes") && response.get("originASes").getAsJsonObject().get("originAS") instanceof JsonObject)
            object.addProperty("origin", response.get("originASes").getAsJsonObject().get("originAS").getAsJsonObject().get("$").getAsString());

        if(response.has("originASes") && response.get("originASes").getAsJsonObject().get("originAS") instanceof JsonArray)
            for(JsonElement x : response.get("originASes").getAsJsonObject().get("originAS").getAsJsonArray())
            {
                String AS = x.getAsJsonObject().get("$").getAsString();
                if(!response.has("origin"))
                    object.addProperty("origin", AS);
                else
                    object.addProperty("origin", object.getProperty("origin") + ", " + AS);
            }

        if(response.has("netBlocks"))
        {
            if(response.get("netBlocks").getAsJsonObject().get("netBlock") instanceof JsonObject)
            {
                JsonObject block = response.get("netBlocks").getAsJsonObject().get("netBlock").getAsJsonObject();
                if (block.has("startAddress") && block.get("startAddress").getAsJsonObject().get("$").getAsString().contains("."))
                    object.addProperty("route", block.get("startAddress").getAsJsonObject().get("$").getAsString() + "/" +
                            block.get("cidrLength").getAsJsonObject().get("$").getAsString());
                if (block.has("startAddress") && block.get("startAddress").getAsJsonObject().get("$").getAsString().contains(":"))
                    object.addProperty("route6", block.get("startAddress").getAsJsonObject().get("$").getAsString() + "/" +
                            block.get("cidrLength").getAsJsonObject().get("$").getAsString());

                results.add(object);
            }
            else if(response.get("netBlocks").getAsJsonObject().get("netBlock") instanceof JsonArray)
            {
                JsonArray blocks = response.get("netBlocks").getAsJsonObject().get("netBlock").getAsJsonArray();
                for(JsonElement x : blocks)
                {
                    JsonObject block = (JsonObject)x;
                    RPSLObject result = new RPSLObject(object.toJson().toString());
                    if (block.has("startAddress") && block.get("startAddress").getAsJsonObject().get("$").getAsString().contains("."))
                        result.addProperty("route", block.get("startAddress").getAsJsonObject().get("$").getAsString() + "/" +
                                block.get("cidrLength").getAsJsonObject().get("$").getAsString());
                    if (block.has("startAddress") && block.get("startAddress").getAsJsonObject().get("$").getAsString().contains(":"))
                        result.addProperty("route6", block.get("startAddress").getAsJsonObject().get("$").getAsString() + "/" +
                                block.get("cidrLength").getAsJsonObject().get("$").getAsString());

                    results.add(result);
                }
            }
        }

        return results;
    }
}
