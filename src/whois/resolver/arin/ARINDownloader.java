package whois.resolver.arin;

import po.POWriter;
import rpsl.RPSLObject;
import rpsl.RPSLReader;
import whois.resolver.Sanitizer;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.time.Instant;
import java.time.ZoneOffset;
import java.time.format.DateTimeFormatter;

public class ARINDownloader implements Sanitizer
{
    public static void download() throws Exception
    {
        Instant time = Instant.now();
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd").withZone(ZoneOffset.UTC);

        /* -----------------------------------------------
         * Create the output file if not existing.
         * -----------------------------------------------*/
        File file = new File("archives/" + formatter.format(time) + "/irr/po/ARIN-BULK.po");
        if(file.exists())
            return;

        file.getParentFile().mkdirs();
        file.createNewFile();

        /* -----------------------------------------------
         * Download the file and start processing.
         * -----------------------------------------------*/
        System.out.println("[" + Instant.now().toString() + "] Downloading and processing ARIN BULK");
        try
        {
            URL url = new URL("https://arin.net/public/secure/downloads/bulkwhois/nets.txt?apikey=API-2F3C-6140-4D62-BDCE");
            HttpURLConnection connection = (HttpURLConnection) url.openConnection();
            DataInputStream stream = new DataInputStream(connection.getInputStream());

            RPSLReader reader = new RPSLReader(stream);
            POWriter writer = new POWriter(new DataOutputStream(new FileOutputStream(file)));

            RPSLObject entry = reader.read();
            while(entry != null)
            {
                RPSLObject filtered = ARINUtility.convertToRPSL(entry);
                if(filtered.hasProperty("route") && filtered.hasProperty("origin"))
                {
                    String[] split = filtered.getProperty("route").split("/");
                    for(String AS : filtered.getProperty("origin").split(","))
                        writer.write(split[0], Integer.parseInt(split[1]), Long.parseLong(AS.trim()));
                }

                if(filtered.hasProperty("route6") && filtered.hasProperty("origin"))
                {
                    String[] split = filtered.getProperty("route6").split("/");
                    for(String AS : filtered.getProperty("origin").split(","))
                        writer.write(split[0], Integer.parseInt(split[1]), Long.parseLong(AS.trim()));
                }

                entry = reader.read();
            }
        }
        catch(Exception e)
        {
            e.printStackTrace();

            // Delete the file so we can try again on subsequent executions.
            file.delete();

            // Throw a new exception causing the system to reboot.
            throw new Exception("Failed to download and process the latest ARIN dump.");
        }

        System.out.println("[" + Instant.now().toString() + "] Done");
    }
}
