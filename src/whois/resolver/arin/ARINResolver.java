package whois.resolver.arin;

import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.google.gson.stream.JsonReader;
import rpsl.RPSLObject;
import whois.resolver.Resolver;

import java.io.InputStreamReader;
import java.net.ConnectException;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;

/**
 * The ARINResolver class can query the WHOIS REST service of ARIN.
 * It includes a rate limiter that automatically adjusts to a lower rate when to many queries are sent.
 */
public class ARINResolver extends Resolver
{
    /** The Limiter that rate limits the amount of requests that are sent to ARIN's REST service. */
    private final static Limiter limiter = new Limiter();

    /**
     * A static limiter class that all ARINResolvers reference to make sure they
     * do not collectively send too many requests.
     */
    private static class Limiter
    {
        /** The amount of requests that are sent in the last period. */
        private Long requests = 0L;

        /** The amount of requests that can be sent per second, starting at 100 */
        private long per_second = 100L;

        /** A timestamp of a certain point in time from which the amount of requests will be tracked. */
        private long timestamp = 0L;

        /**
         * Do a query to ARIN's REST WHOIS service.
         * @param query The query to be done, either an AS123, IPv4 prefix or IPv6 prefix.
         * @return Returns a list of RPSLObjects (containing zero or one element depending on the result.)
         * @throws Exception Any exceptions that occur during the query.
         */
        ArrayList<RPSLObject> query(String query) throws Exception
        {
            while(true)
            {
                synchronized(requests)
                {
                    requests++;

                    // If the timestamp is not set, or if 10 requests have been done remeasure.
                    if (timestamp == 0 && requests % 100 == 0)
                    {
                        timestamp = System.nanoTime();
                        requests = 0L;
                        per_second += 1;
                    }

                    double rps = requests / ((double) (timestamp - System.nanoTime())) / 100000000;

                    if (rps > per_second)
                    {
                        long time = (long) ((-(((double) requests / (double) per_second) - timestamp / 1000000000)) / 1000);
                        System.out.println("ARIN: Time to sleep: " + time + " ms");
                        Thread.sleep(time);
                    }
                }

                URL url;
                if (query.startsWith("AS"))
                    url = new URL("http://whois.arin.net/rest/asn/" + query.replace("AS", "") + ".json");
                else
                    url = new URL(" http://whois.arin.net/rest/cidr/" + query + "/less.json");

                try
                {
                    HttpURLConnection http = (HttpURLConnection) url.openConnection();
                    switch (http.getResponseCode())
                    {
                        case 404:
                            return new ArrayList<>();
                        case 429:
                            per_second -= 1L;
                            break;
                        case 200:

                            JsonReader reader = new JsonReader(new InputStreamReader(url.openStream()));
                            reader.setLenient(true);
                            JsonElement response = new JsonParser().parse(reader);
                            if(!query.startsWith("AS"))
                                return getPrefixes((JsonObject) response);
                            else
                                return ARINUtility.convertToRPSL((JsonObject) response);
                        default:
                            System.out.println("ARIN: Unrecognized response code: " + http.getResponseCode());
                            return new ArrayList<>();
                    }
                }
                catch(ConnectException e)
                {
                    System.out.println("ARIN: " + e.getMessage());
                    e.printStackTrace();
                }
            }
        }

        private ArrayList<RPSLObject> getPrefixes(JsonObject prefixes)
        {
            ArrayList<RPSLObject> list = new ArrayList<>();

            try
            {
                JsonArray nets = new JsonArray();
                JsonElement netRef = prefixes.get("nets").getAsJsonObject().get("netRef");
                if(netRef instanceof JsonObject)
                    nets.add(netRef.getAsJsonObject());
                else
                    nets = prefixes.get("nets").getAsJsonObject().get("netRef").getAsJsonArray();

                boolean status = true;
                for(JsonElement x : nets)
                    while(status)
                    {
                        URL url = new URL(x.getAsJsonObject().get("$").getAsString().replace("\\", "") + ".json");
                        HttpURLConnection http = (HttpURLConnection) url.openConnection();

                        switch (http.getResponseCode())
                        {
                            case 404:
                                status = false;
                                break;
                            case 429:
                                System.out.println("ARIN: Too many requests.");
                                per_second -= 1L;
                                break;
                            case 200:
                                JsonReader reader = new JsonReader(new InputStreamReader(url.openStream()));
                                reader.setLenient(true);
                                JsonElement response = new JsonParser().parse(reader);
                                for(RPSLObject o : ARINUtility.convertToRPSL((JsonObject) response))
                                    list.add(o);

                                status = false;
                                break;
                            default:
                                System.out.println("ARIN: Unrecognized response code: " + http.getResponseCode());
                                status = false;
                                break;
                        }
                    }
            }
            catch(Exception e)
            {
                System.out.println("ARIN: " + e.getMessage());
                e.printStackTrace();
            }

            return list;
        }
    }

    @Override
    public ArrayList<RPSLObject> query(String query) throws Exception
    {
        return limiter.query(query);
    }
}
