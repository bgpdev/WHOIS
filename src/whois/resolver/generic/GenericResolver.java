package whois.resolver.generic;

import rpsl.RPSLObject;
import rpsl.RPSLReader;
import system.Configuration;
import whois.resolver.Resolver;

import java.io.*;
import java.net.*;
import java.util.ArrayList;
import java.util.Date;

/**
 * The WHOIS::GenericResolver class connects to a WHOIS service and performs a query. The connection is terminated automatically.
 * after the query.
 * @author Christian Veenman
 *
 */

public class GenericResolver extends Resolver
{
    /** The URL of the WHOIS service. */
    private final String url;

    /**
     * Construct a WHOIS resolver to a generic whois service.
     * @param url The url of the whois service.
     */
    public GenericResolver(String url)
    {
        this.url = url;
    }

    /**
     * Sets up an connection and blocks till the result is retrieved.
     * @param query The query to be executed on the WHOIS service. A Newline character should not be added.
     */
    public ArrayList<RPSLObject> query(String query) throws Exception
    {
        for(int i = 0; i < Configuration.getConfig().WHOIS_RECONNECT_ATTEMPTS; i++)
        {
            try
            {
                Socket socket = new Socket(url, 43);

                // Send the query to the WHOIS server.
                new DataOutputStream(socket.getOutputStream()).writeBytes(query + '\n');

                // Parse the RPSLObjects in the result.
                RPSLReader reader = new RPSLReader(socket.getInputStream());
                reader.STRICT_PARSING = false;
                ArrayList<RPSLObject> results = reader.readAll();
                socket.close();

                return results;
            }
            catch (Exception e)
            {
                e.printStackTrace();
                System.out.println("[" + new Date().toString() + "] WHOIS: An error occurred: " + e.getMessage());
                System.out.println("[" + new Date().toString() + "] WHOIS: Reconnecting in: " + Configuration.getConfig().WHOIS_RECONNECT_DELAY);
                Thread.sleep(Configuration.getConfig().WHOIS_RECONNECT_DELAY);
            }
        }

        throw new Exception("WHOIS: Max reconnect attempts exceeded.");
    }
}
