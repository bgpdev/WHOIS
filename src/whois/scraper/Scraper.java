package whois.scraper;

import com.google.gson.JsonParser;
import rsef.*;
import system.ElasticSearch;
import whois.resolver.radb.RADBResolver;

import java.io.*;
import java.net.URL;
import java.nio.charset.Charset;
import java.util.*;
import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

/**
 * The Scraper class takes care of coordinating the whole process of scraping the relevant WHOIS databases.
 */
public class Scraper
{
    /**
     * The main entry point of the program.
     * @param argv The arguments passed from the console. Currently not being used.
     * @throws Exception Catches all Exceptions that are not handled internally.
     */
    public static void main(String[] argv) throws Exception
    {
        /* -----------------------------------------------------------------
         * Validate if all the relevant environment settings are specified.
         * ----------------------------------------------------------------- */
        assert System.getenv("ELASTICSEARCH_URL") == null : "Error: ELASTICSEARCH_URL is not specified.";
        assert System.getenv("RADB_CONNECTIONS") == null : "Error: RADB_CONNECTIONS is not specified.";
        assert System.getenv("RADB_RECONNECT_ATTEMPTS") == null : "Error: RADB_RECONNECT_ATTEMPTS is not specified.";
        assert System.getenv("RADB_RECONNECT_DELAY") == null : "Error: RADB_RECONNECT_DELAY is not specified.";
        assert System.getenv("WHOIS_RECONNECT_ATTEMPTS") == null : "Error: WHOIS_RECONNECT_ATTEMPTS is not specified.";
        assert System.getenv("WHOIS_RECONNECT_DELAY") == null : "Error: WHOIS_RECONNECT_DELAY is not specified.";
        assert System.getenv("THREADPOOL_SIZE") == null : "Error: THREADPOOL_SIZE is not specified.";
        assert System.getenv("RIR_EXCHANGE_SPECIFICATIONS") == null : "Error: RIR_EXCHANGE_SPECIFICATIONS is not specified.";
        assert System.getenv("THYME_APNIC_LISTINGS") == null : "Error: THYME_APNIC_LISTINGS is not specified.";
        assert System.getenv("ARIN_FALLBACK") == null : "Error: ARIN_FALLBACK is not specified.";

        /* -----------------------------------------------------------
         * Check if the Elastic Search mappings are already present
         * ----------------------------------------------------------- */
        ElasticSearch elasticsearch = new ElasticSearch(System.getenv("ELASTICSEARCH_URL"));
        if(elasticsearch.get("whois-ipv6/").has("error"))
        {
            System.out.println("[" + new Date().toString() + "] Applying mappings to the database...");
            FileInputStream asn = new FileInputStream(new File("/srv/whois/res/mappings/ASN.json"));
            FileInputStream ipv4 = new FileInputStream(new File("/srv/whois/res/mappings/IPv4.json"));
            FileInputStream ipv6 = new FileInputStream(new File("/srv/whois/res/mappings/IPv6.json"));
            System.out.println(elasticsearch.put("whois-asn/", new JsonParser().parse(new InputStreamReader(asn)).getAsJsonObject()).toString());
            System.out.println(elasticsearch.put("whois-ipv4/", new JsonParser().parse(new InputStreamReader(ipv4)).getAsJsonObject()).toString());
            System.out.println(elasticsearch.put("whois-ipv6/", new JsonParser().parse(new InputStreamReader(ipv6)).getAsJsonObject()).toString());
        }


        /* ----------------------------------------------
         * Read the each RIR Exchange Specifications.
         * ---------------------------------------------- */
        // Parse the environment variables.
        int radb_connections = Integer.parseInt(System.getenv("RADB_CONNECTIONS"));
        int pool_size = Integer.parseInt(System.getenv("THREADPOOL_SIZE"));

        // The ExecutorService (Threadpool) which allows us to do requests in parallel.
        System.out.println("[" + new Date().toString() + "] Creating Threadpool ...");
        ThreadPoolExecutor service = new ThreadPoolExecutor(pool_size,
                pool_size,
                Long.MAX_VALUE,
                TimeUnit.SECONDS,
                new ArrayBlockingQueue<>(pool_size));
        service.setRejectedExecutionHandler(new ThreadPoolExecutor.CallerRunsPolicy());


        /* ----------------------------------------------
         * Construct the RADb connections. (max 5)
         * ---------------------------------------------- */
        System.out.println("[" + new Date().toString() + "] Creating RADb connections ...");
        BlockingQueue<RADBResolver> queue = new ArrayBlockingQueue<>(radb_connections);
        for(int i = 0; i < radb_connections; i++)
            queue.add(new RADBResolver());


        /* ----------------------------------------------
         * Read the each RIR Exchange Specifications.
         * ---------------------------------------------- */
        if(Boolean.parseBoolean(System.getenv("RIR_EXCHANGE_SPECIFICATIONS")))
        {
            List<String> rirs = Arrays.asList(
                    "http://ftp.apnic.net/pub/stats/apnic/delegated-apnic-latest",
                    "http://ftp.lacnic.net/pub/stats/lacnic/delegated-lacnic-latest",
                    "http://ftp.ripe.net/pub/stats/ripencc/delegated-ripencc-latest",
                    "http://ftp.arin.net/pub/stats/arin/delegated-arin-extended-latest",
                    "http://ftp.afrinic.net/pub/stats/afrinic/delegated-afrinic-latest",
                    "http://ftp.apnic.net/stats/iana/delegated-iana-latest");

            // Loop over all the RIR Statistic Exchange Files
            Queue<RSEFRecord> records = new LinkedList<>();
            for (String url : rirs)
            {
                // Construct an RSEFReader to read RSEFObjects from the data source.
                System.out.println("[" + new Date().toString() + "] Downloading: " + url);
                InputStream stream = new URL(url).openStream();
                RSEFReader reader = new RSEFReader(stream);

                RSEFObject x = reader.read();
                while (x != null)
                {
                    if (x instanceof RSEFVersion || x instanceof RSEFSummary)
                        x.print();
                    else
                        records.add((RSEFRecord) x);

                    x = reader.read();
                }
            }

            System.out.println("[" + new Date().toString() + "] Amount of records found: " + records.size());

            while (records.size() != 0)
            {
                RSEFRecord record = records.remove();
                switch (record.type)
                {
                    case "asn":
                        long ASN = Long.parseLong(record.start);
                        service.submit(() -> ASScraper.scrape(ASN, queue, elasticsearch, Boolean.parseBoolean(System.getenv("ARIN_FALLBACK"))));
                        break;
                    case "ipv4":
                        int size = (int) (Math.log(record.value) / Math.log(2));
                        String ipv4 = record.start + "/" + size;
                        service.submit(() -> IPv4Scraper.scrape(ipv4, queue, elasticsearch, Boolean.parseBoolean(System.getenv("ARIN_FALLBACK"))));
                        break;
                    case "ipv6":
                        String ipv6 = record.start + "/" + record.value;
                        service.submit(() -> IPv6Scraper.scrape(ipv6, queue, elasticsearch, Boolean.parseBoolean(System.getenv("ARIN_FALLBACK"))));
                        break;
                }
            }
        }


        /* ----------------------------------------------
         * Read the Thyme ASN and IPv4 listings.
         * ---------------------------------------------- */
        if(Boolean.parseBoolean(System.getenv("THYME_APNIC_LISTINGS")))
        {
            /*
             * Scrape all Autonomous System Numbers.
             */
            {
                ArrayList<Long> asns = new ArrayList<>();
                System.out.println("[" + new Date().toString() + "] Downloading ASN's from http://thyme.apnic.net/current/data-used-autnums ...");
                InputStream stream = new URL("http://thyme.apnic.net/current/data-used-autnums").openStream();
                BufferedReader br = new BufferedReader(new InputStreamReader(stream, Charset.forName("UTF-8")));

                String line;
                while ((line = br.readLine()) != null)
                {
                    String[] split = line.trim().split("\\s", 2);
                    if(split.length == 2 && !split[1].equals("UNKNOWN"))
                        asns.add(Long.parseLong(split[0]));
                }
                br.close();

                System.out.println("[" + new Date().toString() + "] Found " + asns.size() + " ASNs.");
                System.out.println("[" + new Date().toString() + "] Started parsing ASN's ...");
                for (Long asn : asns)
                    service.submit(() -> ASScraper.scrape(asn, queue, elasticsearch, Boolean.parseBoolean(System.getenv("ARIN_FALLBACK"))));
            }

            /*
             * Scrape all IPv4 prefixes.
             */
            {
                ArrayList<String> prefixes = new ArrayList<>();
                System.out.println("[" + new Date().toString() + "] Downloading IPv4 prefixes from http://thyme.apnic.net/current/data-raw-table ...");
                InputStream stream = new URL("http://thyme.apnic.net/current/data-raw-table").openStream();
                BufferedReader br = new BufferedReader(new InputStreamReader(stream, Charset.forName("UTF-8")));

                String line;
                while ((line = br.readLine()) != null)
                    prefixes.add(line.split("\\s", 2)[0]);
                br.close();

                System.out.println("[" + new Date().toString() + "] Found " + prefixes.size() + " IPv4 prefixes.");
                System.out.println("[" + new Date().toString() + "] Started parsing IPv4 addresses ...");
                for (String prefix : prefixes)
                    service.submit(() -> IPv4Scraper.scrape(prefix, queue, elasticsearch, Boolean.parseBoolean(System.getenv("ARIN_FALLBACK"))));
            }
        }


        // Wait till all threads are done.
        service.shutdown();
        service.awaitTermination(Long.MAX_VALUE, TimeUnit.NANOSECONDS);

        /*
         * Close all RADb connections.
         */
        for(int i = 0; i < radb_connections; i++)
            queue.take().close();

        System.out.println("[" + new Date().toString() + "] Finished scraping!");
    }
}