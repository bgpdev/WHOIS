package whois.scraper;

import hex.HexConverter;
import rpsl.RPSLObject;
import system.ElasticSearch;
import whois.resolver.arin.ARINResolver;
import whois.resolver.radb.RADBResolver;

import java.nio.charset.StandardCharsets;
import java.security.MessageDigest;
import java.util.ArrayList;
import java.util.concurrent.BlockingQueue;

public class ASScraper
{
    /**
     * Scrape one ASN number of the RADb database.
     * @param ASN The Autonomous System Number that is to be queried.
     * @param queue The Queue from which they can retrieve a RADBResolver
     * @param engine The ElasticSearch engine that is used to insert results in the database.
     * @param checkARIN Specifies whether ARIN should be queried when RADb does not return a result.
     * @return The results of the WHOIS query.
     */
    public static ArrayList<RPSLObject> scrape(final long ASN,
                                               BlockingQueue<RADBResolver> queue,
                                               ElasticSearch engine,
                                               boolean checkARIN)
    {
        try
        {
            RADBResolver resolver = queue.take();
            ArrayList<RPSLObject> results = resolver.query("-T aut-num AS" + ASN);
            queue.put(resolver);

            for(RPSLObject x : results)
            {
                // TODO: Do this to prevent entries in lower case.
                x.addProperty("aut-num", x.getProperty("aut-num").toUpperCase());

                if(!x.hasProperty("aut-num") || !x.hasProperty("source"))
                    break;

                String x_autnum = x.getProperty("aut-num").trim();
                String x_source = x.getProperty("source").trim();

                MessageDigest digest = MessageDigest.getInstance("SHA-256");
                byte[] hash = digest.digest(x.toJson().toString().getBytes(StandardCharsets.UTF_8));
                String id = x_autnum.toUpperCase() + "|" + x_source + "|" + HexConverter.toHex(hash);
                System.out.println(id);
                engine.put("whois-asn/ASN/" + id, x.toJson());
            }

            // If no results are found in the RADb WHOIS database, query ARIN.
            if(results.size() == 0 && checkARIN)
                return queryArin(ASN, engine);
            else
                return results;
        }
        catch (Exception e)
        {
            e.printStackTrace();
            System.exit(-1);
            return null;
        }
    }

    /**
     * Queries the ARIN WHOIS database and inserts it in Elastic Search.
     * @param ASN The Autonomous System Number that is to be queried.
     */
    private static ArrayList<RPSLObject> queryArin(final long ASN, ElasticSearch engine)
    {
        try
        {
            ArrayList<RPSLObject> result = new ARINResolver().query("AS" + Long.toString(ASN));
            if(result.isEmpty())
                return new ArrayList<>();

            ArrayList<RPSLObject> response = new ArrayList<>();
            for(RPSLObject filtered : result)
            {
                MessageDigest digest = MessageDigest.getInstance("SHA-256");
                byte[] hash = digest.digest(filtered.toJson().toString().getBytes(StandardCharsets.UTF_8));
                String id = filtered.getProperty("aut-num").toUpperCase() + "|ARIN-Filtered|" + HexConverter.toHex(hash);
                System.out.println(id);
                engine.put("whois-asn/ASN/" + id, filtered.toJson());

                if(filtered.getProperty("aut-num").toUpperCase().equals("AS" + ASN))
                    response.add(filtered);
            }

            return response;
        }
        catch(Exception e)
        {
           e.printStackTrace();
           System.exit(-1);
           return null;
        }
    }
}
