package whois.scraper;

import hex.HexConverter;
import network.entities.IP;
import rpsl.RPSLObject;
import system.ElasticSearch;
import whois.resolver.arin.ARINResolver;
import whois.resolver.radb.RADBResolver;

import java.nio.charset.StandardCharsets;
import java.security.MessageDigest;
import java.util.ArrayList;
import java.util.concurrent.BlockingQueue;

public class IPv6Scraper
{
    /**
     * Scrapes all IPv6 numbers of the RADb database.
     * @param prefix The prefix that is to be queried.
     * @param queue The Queue from which they can retrieve a RADBResolver
     * @param engine The ElasticSearch engine that is used to insert results in the database.
     * @param checkARIN Specifies whether ARIN should be queried when RADb does not return a result.
     * @return The results of the WHOIS query.
     */
    public static ArrayList<RPSLObject> scrape(String prefix,
                                               BlockingQueue<RADBResolver> queue,
                                               ElasticSearch engine,
                                               boolean checkARIN)
    {
        try
        {
            RADBResolver resolver = queue.take();
            ArrayList<RPSLObject> results = resolver.query("-T route6 " + prefix);
            queue.put(resolver);

            for(RPSLObject x : results)
            {
                // In case ARIN results are retrieved without route object.
                // TODO: Parse this as an inetnum.

                if(!x.hasProperty("route6") || !x.hasProperty("source"))
                    break;

                // Calculate intermediary values
                String x_prefix = x.getProperty("route6").trim();
                String x_ip = x.getProperty("route6").trim().split("/")[0];
                int x_length = Integer.parseInt(x.getProperty("route6").trim().split("/")[1]);
                String x_source = x.getProperty("source").trim();

                // Add the extra prefix-start and prefix-end.
                x.addProperty("prefix-start", x_ip);
                x.addProperty("prefix-end", IP.getEndingIPv6(x_ip, x_length));

                // Calculate the HASH of the object to keep track of versions.
                MessageDigest digest = MessageDigest.getInstance("SHA-256");
                byte[] hash = digest.digest(x.toJson().toString().getBytes(StandardCharsets.UTF_8));
                String id = x_prefix.replaceAll("/", "-") + "|" + x_source + "|" + HexConverter.toHex(hash);
                System.out.println(id);
                engine.put("whois-ipv6/IPv6/" + id, x.toJson());
            }

            // If no results are found in the RADb WHOIS database, query ARIN.
            if(results.size() == 0 && checkARIN)
                return queryArin(prefix, engine);
            else
                return results;
        }
        catch(Exception exception)
        {
            exception.printStackTrace();
            System.exit(-1);
            return null;
        }
    }

    /**
     * Queries the ARIN WHOIS database and inserts it in Elastic Search.
     * @param prefix The IPv4 prefix that is to be queried.
     */
    private static ArrayList<RPSLObject> queryArin(String prefix, ElasticSearch engine)
    {
        try
        {
            ArrayList<RPSLObject> result = new ARINResolver().query(prefix);
            if(result.isEmpty())
                return new ArrayList<>();

            RPSLObject filtered = result.get(0);

            // Add the extra prefix-start and prefix-end.
            filtered.addProperty("prefix-start", filtered.getProperty("route6").split("/")[0]);
            filtered.addProperty("prefix-end",
                    IP.getEndingIPv6(
                            filtered.getProperty("route6").split("/")[0],
                            Integer.parseInt(filtered.getProperty("route6").split("/")[1])));

            // Calculate the HASH of the object to keep track of versions.
            MessageDigest digest = MessageDigest.getInstance("SHA-256");
            byte[] hash = digest.digest(filtered.toJson().toString().getBytes(StandardCharsets.UTF_8));
            String id = filtered.getProperty("route6").replaceAll("/", "-") + "|ARIN-Filtered|" + HexConverter.toHex(hash);
            System.out.println(id);
            engine.put("whois-ipv6/IPv6/" + id, filtered.toJson());

            return result;
        }
        catch(Exception e)
        {
            e.printStackTrace();
            System.exit(-1);
            return null;
        }
    }
}
