package cache;

import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import rpsl.RPSLObject;
import system.Configuration;
import system.ElasticSearch;
import whois.resolver.radb.RADBResolver;
import whois.scraper.ASScraper;
import whois.scraper.IPv4Scraper;
import whois.scraper.IPv6Scraper;

import java.util.ArrayList;
import java.util.WeakHashMap;
import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.BlockingQueue;

/**
 * The Cache object ties all the WHOIS databases together. It performs:
 * - Caching of WHOIS results for a variable amount of time.
 * - Scraping multiple databases in search of the right result (RIPE, RADB, ...) in case something has not been found.
 * - Holds a Queue such that multiple queries can be done at the same time.
 *
 * The service is constructed of 3 layers:
 * - Local Cache                (Oldest Time entries should be cleaned upon having too little memory.
 * - Elastic Search Database
 * - RADB WHOIS Query + ARIN
 */
public class Cache
{
    // The WHOIS Cache.Cache which stores the results.
    private WeakHashMap<String, ArrayList<RPSLObject>> cache = new WeakHashMap<>();

    // The ElasticSearch Engine which will be used when the Cache.Cache has a Cache.Cache miss.
    private ElasticSearch elasticsearch;

    // The queue of RADbResolvers for quick resolving.
    private BlockingQueue<RADBResolver> queue = new ArrayBlockingQueue<>(5);

    // The static WHOIS Cache object that keeps track of the requests being done to WHOIS services.
    private static Cache service = null;

    /**
     * Constructor of the WHOISService.
     */
    private Cache()
    {
        try
        {
            System.out.println("[WHOIS::Cache] Constructing the RADb connections...");
            for(int i = 0; i < 5; i++)
                queue.add(new RADBResolver());
            elasticsearch = new ElasticSearch(Configuration.getConfig().ELASTICSEARCH_URL);
        }
        catch(Exception e)
        {
            e.printStackTrace();
            System.exit(-1);
        }
    }

    public static Cache getCache()
    {
        if(service == null)
            service = new Cache();

        return service;
    }

    public ArrayList<RPSLObject> query(String identifier, boolean refresh)
    {
        try
        {
            if(!refresh)
            {
                /* ---------------------------------------------------
                 * #1: Check if any entries exist in the local cache.
                 * ---------------------------------------------------*/
                if (cache.containsKey(identifier))
                    return cache.get(identifier);

                /* ---------------------------------------------------------------------
                 * #2: Check if any entries exist in the local Elastic Search database.
                 * ---------------------------------------------------------------------*/
                JsonObject result = null;

                String ip = identifier.split("/")[0];

                try
                {
                    if (identifier.startsWith("AS"))
                        result = elasticsearch.get("whois-asn/_search?q=aut-num:" + identifier + "&analyzer=keyword").get("hits").getAsJsonObject();
                    else if (identifier.contains("."))
                        result = elasticsearch.get("whois-ipv4/_search?q=prefix-start:[*%20TO%20\"" + ip + "\"]%20AND%20prefix-end:[\"" + ip + "\"%20TO%20*]").get("hits").getAsJsonObject();
                    else if (identifier.contains(":"))
                        result = elasticsearch.get("whois-ipv6/_search?q=prefix-start:[*%20TO%20\"" + ip + "\"]%20AND%20prefix-end:[\"" + ip + "\"%20TO%20*]").get("hits").getAsJsonObject();
                    else
                        throw new Exception("Invalid identifier: " + identifier);
                }
                catch(Exception e)
                {
                    e.printStackTrace();
                }

                if (result.get("total").getAsInt() != 0)
                {
                    ArrayList<RPSLObject> response = new ArrayList<>();
                    for (JsonElement x : result.get("hits").getAsJsonArray())
                        response.add(new RPSLObject(x.getAsJsonObject().get("_source").getAsJsonObject().toString()));

                    // Add the results to the Cache.Cache to speed up further requests.
                    cache.put(identifier, response);
                    return response;
                }
            }

            /* ---------------------------------------
             * #3: Check the RADB and ARIN database.
             * ---------------------------------------*/
            ArrayList<RPSLObject> results;
            if(identifier.startsWith("AS"))
                results = ASScraper.scrape(Long.parseLong(identifier.replace("AS", "")), queue, elasticsearch, true);
            else if(identifier.contains("."))
                results = IPv4Scraper.scrape(identifier, queue, elasticsearch, true);
            else
                results = IPv6Scraper.scrape(identifier, queue, elasticsearch, true);

            // Insert the results of the query in the Cache.Cache for further queries.
            cache.put(identifier, results);

            return results;

        }
        catch (Exception e)
        {
            e.printStackTrace();
            System.exit(-1);
            return null;
        }
    }
}
